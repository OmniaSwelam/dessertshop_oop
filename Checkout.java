import java.util.ArrayList;
import java.util.Collections;

public class Checkout {
	//private int cost;
	private String receipt;
	private ArrayList<dessertItem> dessertItems;
	
	public Checkout() {
		dessertItems= new ArrayList<dessertItem>();
	}
	
	public void enterItem(dessertItem item) {
		dessertItems.add(item);
	}

	public int numberOfItems() {
		return dessertItems.size();
	}
	
	public void clear() {
		dessertItems.clear();
	}

	//Returns total cost of items in cents (without tax)
	public int totalCost() {
		int cost=0;
		for(int i=0; i<dessertItems.size(); i++) {
			cost+= dessertItems.get(i).getCost();
		}
		return cost;
	}
	
	//Returns total tax on items in cents
	public int totalTax() {
		return  (int) Math.round((dessertShop.TAX_RATE /100)*totalCost());
	}

	public String toString() {
		//receipt= "   "+ dessertShop.STORE_NAME+ "   ";
	    StringBuilder sb = new StringBuilder();
	    sb.append("	"+ dessertShop.STORE_NAME+ "	\n");
	    sb.append("        --------------------     "+"\n");
	    sb.append("\n");
	    
	    for(int i=0;i<dessertItems.size(); i++) {
	    	if (dessertItems.get(i) instanceof Candy) {
	    		Candy c= (Candy) dessertItems.get(i);
	    		sb.append(c.getWeightInPound()+ " lbs. @ "+ dessertShop.cents2dollarsAndCents(c.getPricePerPound())+" /lb. \n");
		    	//sb.append(dessertItems.get(i).getName()+ "\n");
	    	}
	    	else if (dessertItems.get(i) instanceof Cookie) {
	    		Cookie c= (Cookie) dessertItems.get(i);
	    		sb.append(c.getNumber()+ " @ "+ dessertShop.cents2dollarsAndCents((int) c.getPricePerDozen())+" /dz. \n");
		    	//sb.append(dessertItems.get(i).getName()+ "\n");

	    	}
	    	else if (dessertItems.get(i) instanceof Sundae) {
	    		Sundae s= (Sundae) dessertItems.get(i);
	    		sb.append(s.getToppingName()+ " Sundae with \n");
	    	}

	    	sb.append(dessertItems.get(i).getName()+ String.join("", Collections.nCopies(30- dessertItems.get(i).getName().length(), " ")));
	    	sb.append(dessertShop.cents2dollarsAndCents(dessertItems.get(i).getCost())+ "\n");
	    }
	    
	    sb.append("\n");
	    sb.append("Tax" +String.join("", Collections.nCopies(30- "Tax".length(), " "))+ dessertShop.cents2dollarsAndCents(totalTax())+"\n");
	    sb.append("Total Cost"+String.join("", Collections.nCopies(30- "Total Cost".length(), " ")) + dessertShop.cents2dollarsAndCents(totalCost()+ totalTax()));
	    
	    return sb.toString();
	    
	}


}
