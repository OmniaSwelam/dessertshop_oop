
public class IceCream extends dessertItem {
	protected int price;
	
	public IceCream(String name, int price) {
		super(name);
		this.price=price;
	}
	
	public int getCost() {
		return price;
	}
}
