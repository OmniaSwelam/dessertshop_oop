public abstract class dessertItem {
  
  protected String name;
  

  /*public dessertItem() {
    this("");
  }*/
/**
 * Initializes DessertItem data
 */   
  public dessertItem(String name) {
    if (name.length() <= dessertShop.MAX_ITEM_NAME_SIZE)
      this.name = name;
    else 
      this.name = name.substring(0,dessertShop.MAX_ITEM_NAME_SIZE);
  }

  public final String getName() {
    return name;
  }
 
  public abstract int getCost();

}