
public class Sundae extends IceCream {
	private final String toppingName;
	private float toppingCost;
	
	public Sundae(String name, int price, String n, float c) {
		super(name,price);
		this.toppingName=n;
		this.toppingCost= c;
	}
	@Override
	public int getCost() {
		float cost= price+ toppingCost;
		return Math.round(cost);
	}
	public String getToppingName() {
		return toppingName;
	}
}
