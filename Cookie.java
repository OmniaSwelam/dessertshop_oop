
public class Cookie extends dessertItem {
	
	private int number;
	private float pricePerDozen;
	
	public Cookie(String name, int n, float p) {
		super(name);
		this.number=n;
		this.pricePerDozen=p;
	}
	public int getNumber() {
		return number;
	}
	public float getPricePerDozen() {
		return pricePerDozen;
	}
	public int getCost() {
		float cost= (pricePerDozen/12)*number;
		return Math.round(cost);
	}
}
