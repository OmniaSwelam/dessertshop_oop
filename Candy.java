
public class Candy extends dessertItem {
	private final double weightInPound;
	private final int pricePerPound; //in cent
	
	
	public Candy(String name, double w, int p) {
		super(name);
		this.weightInPound=w;
		this.pricePerPound=p;
	}
	public double getWeightInPound() {
		return weightInPound;
	}
	public int getPricePerPound() {
		return pricePerPound;
	}

	public int getCost() {
		double cost= this.weightInPound* this.pricePerPound;
		return (int) Math.round(cost);
	}
}
